
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
# from __future__ import unicode_literals

import sys
sys.path.insert(0, "../input/libs")
from mlmodels import LuongAttnDecoderRNN
from mlmodels import Attn
from mlmodels import EncoderRNN
import math
import itertools
from io import open
import codecs
import unicodedata
import os
import re
import random
import csv
import torch.nn.functional as F
from torch import optim
import torch.nn as nn
from torch.jit import script, trace
import torch



USE_CUDA = torch.cuda.is_available()
device = torch.device("cuda" if USE_CUDA else "cpu")

corpus_name = "finbot"


PAD_token = 0  # Used for padding short sentences
SOS_token = 1  # Start-of-sentence token
EOS_token = 2  # End-of-sentence token


class Voc:
    def __init__(self, name):
        self.name = name
        self.trimmed = False
        self.word2index = {}
        self.word2count = {}
        self.index2word = {PAD_token: "PAD",
                           SOS_token: "SOS", EOS_token: "EOS"}
        self.num_words = 3  # Count SOS, EOS, PAD

    def addSentence(self, sentence):
        for word in sentence.split(' '):
            self.addWord(word)

    def addWord(self, word):
        if word not in self.word2index:
            self.word2index[word] = self.num_words
            self.word2count[word] = 1
            self.index2word[self.num_words] = word
            self.num_words += 1
        else:
            self.word2count[word] += 1

    # Remove words below a certain count threshold
    def trim(self, min_count):
        if self.trimmed:
            return
        self.trimmed = True

        keep_words = []

        for k, v in self.word2count.items():
            if v >= min_count:
                keep_words.append(k)

        print('keep_words {} / {} = {:.4f}'.format(
            len(keep_words), len(self.word2index), len(
                keep_words) / len(self.word2index)
        ))

        # Reinitialize dictionaries
        self.word2index = {}
        self.word2count = {}
        self.index2word = {PAD_token: "PAD",
                           SOS_token: "SOS", EOS_token: "EOS"}
        self.num_words = 3  # Count default tokens

        for word in keep_words:
            self.addWord(word)


MAX_LENGTH = 15  # Maximum sentence length to consider


voc = Voc(corpus_name)

MIN_COUNT = 3    # Minimum word count threshold for trimming


def indexesFromSentence(voc, sentence):

    # print(sentence)
    cleansentece = []
    for word in sentence.split(' '):

        if word in voc.word2index:    
           cleansentece.append(word) 

    # return [voc.word2index[word] for word in sentence.split(' ')] + [EOS_token]
    return [voc.word2index[word] for word in cleansentece] + [EOS_token]



class GreedySearchDecoder(nn.Module):
    def __init__(self, encoder, decoder):
        super(GreedySearchDecoder, self).__init__()
        self.encoder = encoder
        self.decoder = decoder

    def forward(self, input_seq, input_length, max_length):
        # Forward input through encoder model
        encoder_outputs, encoder_hidden = self.encoder(input_seq, input_length)
        # Prepare encoder's final hidden layer to be first hidden input to the decoder
        decoder_hidden = encoder_hidden[:decoder.n_layers]
        # Initialize decoder input with SOS_token
        decoder_input = torch.ones(
            1, 1, device=device, dtype=torch.long) * SOS_token
        # Initialize tensors to append decoded words to
        all_tokens = torch.zeros([0], device=device, dtype=torch.long)
        all_scores = torch.zeros([0], device=device)
        # Iteratively decode one word token at a time
        for _ in range(max_length):
            # Forward pass through decoder
            decoder_output, decoder_hidden = self.decoder(
                decoder_input, decoder_hidden, encoder_outputs)
            # Obtain most likely word token and its softmax score
            decoder_scores, decoder_input = torch.max(decoder_output, dim=1)
            # Record token and score
            all_tokens = torch.cat((all_tokens, decoder_input), dim=0)
            all_scores = torch.cat((all_scores, decoder_scores), dim=0)
            # Prepare current token to be next decoder input (add a dimension)
            decoder_input = torch.unsqueeze(decoder_input, 0)
        # Return collections of word tokens and scores
        return all_tokens, all_scores


def evaluate(encoder, decoder, searcher, voc, sentence, max_length=MAX_LENGTH):
    # Format input sentence as a batch
    # words -> indexes
    indexes_batch = [indexesFromSentence(voc, sentence)]
    # Create lengths tensor
    lengths = torch.tensor([len(indexes) for indexes in indexes_batch])
    # Transpose dimensions of batch to match models' expectations
    input_batch = torch.LongTensor(indexes_batch).transpose(0, 1)
    # Use appropriate device
    input_batch = input_batch.to(device)
    lengths = lengths.to(device)
    # Decode sentence with searcher
    tokens, scores = searcher(input_batch, lengths, max_length)
    # indexes -> words
    decoded_words = [voc.index2word[token.item()] for token in tokens]
    return decoded_words


def evaluateInput(encoder, decoder, searcher, voc):

    # input_sentences = ['soita mulle', 'missa sina asut',
    #                    'sovitaan hinta', 'milloin voidaan tavata', 'kaikki ihan hyvin','millon oot viimeksi nainut','mitä tua tarkota','mitä tua tarkoittaa']

    input_sentences = []

    filepath = '../input/inputsentences.txt'  
    with open(filepath) as fp:  
        line = fp.readline()
        while line:
            input_sentences.append(line.strip())
            line = fp.readline()
       
    with open('txtout.txt', 'a') as file:
        for sent in input_sentences:
            # print(sent)
            file.write("Asiakas: "+sent+"\n")
            output_words = evaluate(encoder, decoder, searcher, voc, sent)
            output_words[:] = [x for x in output_words if not (
            x == 'EOS' or x == 'PAD')]

            outphrases =[]
            cnt = 0
            point = False
            pointstr ='.'
            for wrd in output_words:
                if cnt == 0 or  point is True:
                    if cnt > 1:
                       outphrases[cnt-1]=''
                       outphrases[cnt-2]=outphrases[cnt-2] +pointstr

                    outphrases.append(wrd.capitalize())
 
                    point = False
                else:
                   outphrases.append(wrd)

                cnt += 1
                if wrd =='.' or  wrd =='?':
                  point = True
                  pointstr = wrd  
                    
            outphrase = ' '.join(outphrases)
            outphrase.strip()
            outphrase =outphrase.replace('  ',' ')  
            outphrase =outphrase.replace(' .','.')
            outphrase =outphrase.replace(' ?','?')
            outphrase =outphrase.replace('.  .','.')
            outphrase =outphrase.replace('?  .','?')
            outphrase =outphrase.replace('..','.')
            outphrase.strip()

            file.write("Robotti: "+ outphrase+'\n\n')




# model_name = 'cb_model'
attn_model = 'dot'
#attn_model = 'general'
#attn_model = 'concat'
hidden_size = 500
encoder_n_layers = 2
decoder_n_layers = 2
dropout = 0.1
# batch_size = 128

loadFilename = '../input/model0.pt'

checkpoint = torch.load(loadFilename)
    # If loading a model trained on GPU to CPU
    #checkpoint = torch.load(loadFilename, map_location=torch.device('cpu'))
encoder_sd = checkpoint['en']
decoder_sd = checkpoint['de']
encoder_optimizer_sd = checkpoint['en_opt']
decoder_optimizer_sd = checkpoint['de_opt']
embedding_sd = checkpoint['embedding']
voc.__dict__ = checkpoint['voc_dict']


print('Building encoder and decoder ...')
# Initialize word embeddings
embedding = nn.Embedding(voc.num_words, hidden_size)

embedding.load_state_dict(embedding_sd)
# Initialize encoder & decoder models
encoder = EncoderRNN(hidden_size, embedding, encoder_n_layers, dropout)
decoder = LuongAttnDecoderRNN(
    attn_model, embedding, hidden_size, voc.num_words, decoder_n_layers, dropout)

encoder.load_state_dict(encoder_sd)
decoder.load_state_dict(decoder_sd)
# Use appropriate device
encoder = encoder.to(device)
decoder = decoder.to(device)


# Ensure dropout layers are in train mode
encoder.train()
decoder.train()

print("Start Eval")
encoder.eval()
decoder.eval()
searcher = GreedySearchDecoder(encoder, decoder)
evaluateInput(encoder, decoder, searcher, voc)
